<?php

namespace Jsrx\JdVopSdk\Utils;

use Exception;
use Jsrx\JdVopSdk\Utils\Cache\FileCache;
use Throwable;

/**
 * 日志管理器
 * @class LogManager
 * @package Jsrx\JdVopSdk\Utils
 */
class LogManager
{
    /**
     * 日志配置参数
     * @var array
     */
    protected $options = [
        'log_file'  => '',  // 日志路径
        'log_cycle' => 30,  // 日志周期，超期自动清理(单位:天)
    ];

    /**
     * 日志信息存放
     * @var array
     */
    protected $loggerInfo = [];

    /**
     * 初始化参数
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        // 合并日志配置
        if (!empty($options)) {
            $this->options = array_merge($this->options, $options);
        }

        // 进行历史日志清理
        $this->clearLogs();
    }

    /**
     * 新增日志信息
     * 注意：需要异常包裹，插入失败也不允许影响SDK调用
     * @param $name
     * @param $data
     * @return void
     */
    public function appendLog($name, $data)
    {
        try {
            // 格式化为标准字符串并存入当前类
            $data = $this->dataStringify($data);
            $this->loggerInfo[] = [ 'name' => $name, 'data' => $data ];
        } catch (Throwable $throwable) {
            // 插入失败
        }
    }

    /**
     * 日志信息存储
     * 注意：需要异常包裹，存储失败也不允许影响SDK调用
     * @return void
     */
    public function logSave()
    {
        try {

            // 开启日志时进行日志存储
            if ($logFile = $this->getLogFile()) {
                $logStr = "\n--------------------- JDVopSdk ---------------------\n";
                foreach ($this->loggerInfo as $item) {
                    $logStr .= "{$item['name']}: {$item['data']}" . "\n";
                }

                // 获取日志文件路径并发起存储
                file_put_contents($logFile, $logStr, FILE_APPEND);
            }

        } catch (Throwable $throwable) {
            // 存储失败
        }
    }

    /**
     * 进行日志文件清理
     * 注意：需要异常包裹，清理失败也不允许影响SDK调用
     * @return void
     */
    public function clearLogs()
    {
        try {

            // 日志检查缓存始终使用文件缓存
            $cache = new FileCache;

            // 日志清除标记(清理逻辑60min执行一次)
            $cleanLockFlag = 'JVS-CleanOverFlag';
            if ($cache->has($cleanLockFlag)) {
                return;
            }

            // 获取日志文件列表
            $globPattern = trim(dirname($this->options['log_file']), '/\\') . DIRECTORY_SEPARATOR . '*_' . basename($this->options['log_file']);
            $files = (array)glob($globPattern);

            // 遍历日志文件将超期的文件删除
            $expire = (int)date('Ymd', strtotime("-{$this->options['log_cycle']}days"));
            foreach ($files as $path) {
                $logDate = (int)substr(basename($path), 0, 8);
                if ($logDate < $expire) {
                    unlink($path);
                }
            }

        } catch (Throwable $throwable) {
            // 清理失败
        }
    }

    /**
     * 生成日志文件路径
     * @return false|string
     */
    private function getLogFile()
    {
        // 日志路径为空不予落盘
        if (empty($this->options['log_file'])) {
            return false;
        }

        // 建立日志文件夹
        $logFileDir = dirname($this->options['log_file']);
        if (!is_dir($logFileDir)) {
            @mkdir($logFileDir, 0755, true);
        }

        // 构建日志文件路径
        $logFileName = date('Ymd_') . basename($this->options['log_file']);
        $logFullPath = trim(dirname($this->options['log_file'])) . DIRECTORY_SEPARATOR . $logFileName;
        // 日志文件不存在以0644建立
        if (!is_file($logFullPath)) {
            @touch($logFullPath);
            @chmod($logFullPath, 0644);
        }

        // 返回已准备好的日志文件路径
        return $logFullPath;
    }

    /**
     * 转换为字符串
     * @param $data
     * @return string
     * @throws Exception
     */
    private function dataStringify($data): string
    {
        // 数组或对象类型转换为Json
        if (is_array($data) | is_object($data)) {
            $data = Helper::jsonEncode($data);
        }

        // 其他类型强制为字符串
        return (string)$data;
    }
}