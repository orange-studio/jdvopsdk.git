<?php

namespace Jsrx\JdVopSdk\Service;

use Psr\SimpleCache\InvalidArgumentException;
use Throwable;

/**
 * VOP-库存API
 * @class Stock
 * @package Jsrx\JdVopSdk\Service
 */
class Stock extends BasicService
{
    /**
     * 批量获取库存接口
     * @param $params
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function getNewStockById($params)
    {
        $method = 'jingdong.vop.goods.getNewStockById';
        return $this->sendRequest($method, ['getStockByIdGoodsReq' => $params]);
    }
}