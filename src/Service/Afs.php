<?php

namespace Jsrx\JdVopSdk\Service;

use Psr\SimpleCache\InvalidArgumentException;
use Throwable;

/**
 * VOP-售后API
 * @class   Afs
 * @package Jsrx\JdVopSdk\Service
 */
class Afs extends BasicService
{
    /**
     * 批量查询订单下商品售后权益
     * @param array $reqParams
     * @return bool|mixed
     * @throws Throwable
     */
    public function getGoodsAttributes(array $reqParams)
    {
        $method = 'jingdong.vop.afs.getGoodsAttributes';
        return $this->sendRequest($method, $reqParams);
    }

    /**
     * 申请售后
     * @param array $reqParams
     * @return bool|mixed
     * @throws Throwable
     */
    public function createAfsApply(array $reqParams)
    {
        $method = 'jingdong.vop.afs.createAfsApply';
        return $this->sendRequest($method, ['applyAfterSaleOpenReq' => $reqParams]);
    }

    /**
     * 售后申请单取消
     * @param array $reqParams
     * @return bool|mixed
     * @throws Throwable
     */
    public function cancelAfsApply(array $reqParams)
    {
        $method = 'jingdong.vop.afs.cancelAfsApply';
        return $this->sendRequest($method, $reqParams);
    }

    /**
     * 查询客户寄回地址
     * @param array $reqParams
     * @return bool|mixed
     * @throws Throwable
     */
    public function queryAfsAddressInfos(array $reqParams)
    {
        $method = 'jingdong.vop.afs.queryAfsAddressInfos';
        return $this->sendRequest($method, $reqParams);
    }

    /**
     * 填写运单信息
     * @param array $reqParams
     * @return bool|mixed
     * @throws Throwable
     */
    public function updateSendInfo(array $reqParams)
    {
        $method = 'jingdong.vop.afs.updateSendInfo';
        return $this->sendRequest($method, ['updateAfterSaleWayBillOpenReq' => $reqParams]);
    }

    /**
     * 查询售后单物流信息
     * @param array $reqParams
     * @return bool|mixed
     * @throws Throwable
     */
    public function queryLogicticsInfo(array $reqParams)
    {
        $method = 'jingdong.vop.afs.queryLogicticsInfo';
        return $this->sendRequest($method, $reqParams);
    }

    /**
     * 查询售后概要
     * @param array $reqParams
     * @return bool|mixed
     * @throws Throwable
     */
    public function getAfsOutline(array $reqParams)
    {
        $method = 'jingdong.vop.afs.getAfsOutline';
        return $this->sendRequest($method, $reqParams);
    }

    /**
     * 查询售后退款明细
     * @param array $reqParams
     * @return bool|mixed
     * @throws Throwable
     */
    public function findRefundInfo(array $reqParams)
    {
        $method = 'jingdong.vop.afs.findRefundInfo';
        return $this->sendRequest($method, $reqParams);
    }

    /**
     * 确认售后服务单
     * @param array $reqParams
     * @return bool|mixed
     * @throws Throwable
     */
    public function confirmAfsOrder(array $reqParams)
    {
        $method = 'jingdong.vop.afs.confirmAfsOrder';
        return $this->sendRequest($method, $reqParams);
    }

    /**
     * 获取组件信息
     * @param array $reqParams
     * @return bool|mixed
     * @throws Throwable
     */
    public function getComponentUrl(array $reqParams)
    {
        $method = 'jingdong.vop.afs.getComponentUrl';
        return $this->sendRequest($method, $reqParams);
    }
}