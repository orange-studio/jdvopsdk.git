<?php

namespace Jsrx\JdVopSdk\Service;

use Psr\SimpleCache\InvalidArgumentException;
use Throwable;

/**
 * BBC-通用场景接口
 * @class Bbc
 * @package Jsrx\JdVopSdk\Service
 */
class Bbc extends BasicService
{
    /**
     * 用户中心-批量查询成员
     * @param array $reqParams
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function batchListUser(array $reqParams)
    {
        $method = 'jingdong.batchListUser';
        return $this->sendRequest($method, $reqParams);
    }

    /**
     * 用户中心-查询用户
     * @param array $reqParams
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function getUserById(array $reqParams)
    {
        $method = 'jingdong.getUserById';
        return $this->sendRequest($method, $reqParams);
    }

    /**
     * 用户中心-删除成员
     * @param int $userId
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function deleteUserById(int $userId)
    {
        $method = 'jingdong.deleteUserById';
        return $this->sendRequest($method, compact('userId'));
    }

    /**
     * 用户中心-更新成员
     * @param array $reqParams
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function updateUser(array $reqParams)
    {
        $method = 'jingdong.updateUser';
        return $this->sendRequest($method, $reqParams);
    }

    /**
     * 用户中心-批量添加成员
     * @param array $reqParams
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function batchAddUsers(array $reqParams)
    {
        $method = 'jingdong.batchAddUsers';
        return $this->sendRequest($method, $reqParams);
    }

    /**
     * 用户中心-批量删除成员
     * @param array $reqParams
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function batchDeleteUsers(array $reqParams)
    {
        $method = 'jingdong.batchDeleteUsers';
        return $this->sendRequest($method, $reqParams);
    }

    /**
     * 用户中心-批量更新成员
     * @param array $reqParams
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function batchUpdateUser(array $reqParams)
    {
        $method = 'jingdong.batchUpdateUser';
        return $this->sendRequest($method, $reqParams);
    }

    /**
     * 积分管理-批量查询用户积分
     * @param array $reqParams
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function batchQueryAsset(array $reqParams)
    {
        $method = 'jingdong.batchQueryAsset';
        return $this->sendRequest($method, $reqParams);
    }

    /**
     * 积分管理-批量增减用户积分
     * @param array $reqParams
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function batchAddOrUpdateAsset(array $reqParams)
    {
        $method = 'jingdong.batchAddOrUpdateAsset';
        return $this->sendRequest($method, $reqParams);
    }

    /**
     * 用户中心-新增成员
     * @param array $reqParams
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function createUser(array $reqParams)
    {
        $method = 'jingdong.createUser';
        return $this->sendRequest($method, $reqParams);
    }

    /**
     * 创建/编辑活动
     * @param array $reqParams
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function bbcAddOrUpdateActivity(array $reqParams)
    {
        $method = 'jingdong.bbcAddOrUpdateActivity';
        return $this->sendRequest($method, $reqParams);
    }

    /**
     * 积分管理-增减用户积分
     * @param float $assets
     * @param string $personId
     * @param int $type
     * @param string $projectId
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function addOrUpdateAsset(float $assets, string $personId, int $type, string $projectId)
    {
        $method = 'jingdong.addOrUpdateAsset';
        return $this->sendRequest($method, compact('assets', 'personId', 'type', 'projectId'));
    }

    /**
     * 积分管理-查询用户积分
     * @param string $personId
     * @param int $type
     * @param string $projectId
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function queryOneAsset(string $personId, int $type, string $projectId)
    {
        $method = 'jingdong.queryOneAsset';
        return $this->sendRequest($method, compact('personId', 'type', 'projectId'));
    }

    /**
     * 分页获取活动列表
     * @param array $reqParams
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function bbcActivityPageList(array $reqParams)
    {
        $method = 'jingdong.bbcActivityPageList';
        return $this->sendRequest($method, $reqParams);
    }

    /**
     * 开启/关闭活动
     * @param string $activityCode
     * @param int $activityState
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function bbcChangeActivityState(string $activityCode, int $activityState)
    {
        $method = 'jingdong.bbcChangeActivityState';
        return $this->sendRequest($method, compact('activityCode', 'activityState'));
    }

    /**
     * 获取活动详情
     * @param string $activityCode
     * @return bool|mixed
     * @throws InvalidArgumentException
     * @throws Throwable
     */
    public function bbcQueryActivityDetails(string $activityCode)
    {
        $method = 'jingdong.bbcQueryActivityDetails';
        return $this->sendRequest($method, compact('activityCode'));
    }
}