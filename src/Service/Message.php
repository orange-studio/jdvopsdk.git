<?php

namespace Jsrx\JdVopSdk\Service;


use Psr\SimpleCache\InvalidArgumentException;
use Throwable;

/**
 * API授权接口
 * @class Accesstoken
 * @package Jsrx\JdVopSdk\Service
 */
class Message extends BasicService
{
    /**
     * @throws Throwable
     * @throws InvalidArgumentException
     */
    public function deleteClientMsgByIdList($id)
    {
        $method = 'jingdong.vop.message.deleteClientMsgByIdList';
        return $this->sendRequest($method, compact('id'));
    }

    /**
     * @throws Throwable
     * @throws InvalidArgumentException
     */
    public function queryTransByVopNormal($type)
    {
        $method = 'jingdong.vop.message.queryTransByVopNormal';
        return $this->sendRequest($method, compact('type'));
    }
}